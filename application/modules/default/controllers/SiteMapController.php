<?php
class Default_SiteMapController extends Custom_Controller_Action_InitializedCompany {
	
	public function init() {
		parent::init();
		
		$user = $this->getUser();
		if ($user->isCreator() && $this->view->hasBackupData()) {
			$this->_redirectSimple('index', 'index');
		}
	}
	
	public function indexAction() {
		$this->view->headTitle('ページの作成/更新');
		$this->view->topicPath('ページの作成/更新');

		/** @var App_Model_DbTable_Hp_Row $hp */
		$hp = $this->getUser()->getCurrentHp();
        $company = $hp->fetchCompanyRow();

        $isTopOriginal = $company->checkTopOriginal();
		
		$table = App_Model_DbTable_HpPage::slave();
		$table->setPageTypeInfoUnique($hp,$isTopOriginal);

		if($isTopOriginal){
            $pageList = $table->getTypeListJp();
            $pageList[$table::TYPE_TOP] = App_Model_List_Original::TOP_CONTENT;
		    $table->setTypeListJp($pageList);
        }

		$this->view->types = $table->getTypeList();
		$this->view->typeNames = $table->getTypeListJp();
		$this->view->categories = $table->getCategoryList();
		$this->view->categoryNames = $table->getCategories();
		
		$this->view->fixedMenuTypes = $table->getFixedMenuTypeList();
		$this->view->globalMenuTypes = array_values($table->getGlobalMenuTypeList());
		$this->view->globalMenuNumber = $isTopOriginal ? $hp->global_navigation : 6;
		$this->view->notInMenuTypes = array_values($table->getNotInMenuTypeList());
		$this->view->uniqueTypes = $table->getUniqueTypeList();
		
		$this->view->hasDetailPageTypes = $table->getHasDetailPageTypeList();
		$this->view->detailPageTypes = $table->getDetailPageTypeList();
		$this->view->hasMultiPageTypes = $table->getHasMultiPageTypeList();
		$this->view->childTypes = $table->getAllChildTypesByType();
		$this->view->categoryMap		= $table->getCategoryMap()				;
		
		if ( $company->cms_plan >= App_Model_List_CmsPlan::CMS_PLAN_ADVANCE )
		{	// TODO:ここ何とかしなきゃ（新しいプランとか色々影響が出る）
			$table->checkNewTemplateData(	$hp->id		) ;			// 新規追加テンプレートチェック
			$table->checkEstateRequest(		$hp->id		) ;			// 物件リクエストチェック
		}
		
        $this->view->cmsPlan = $this->getUser()->getProfile()->cms_plan;
		$this->view->siteMapData = $table->fetchSiteMapRows($hp->id)->toSiteMapArray();
		if ($setting = $hp->getEstateSetting()) {
			$this->view->estateSiteMapData = $setting->toSiteMapData();
		}

		$this->view->isTopOriginal = (int)$isTopOriginal;
		$this->view->isAgency = (int)$this->getUser()->isAgency();

        if ($this->view->isTopOriginal && !$this->view->isAgency) {
            $this->view->globalNav = $hp->getGlobalNavigation()->toSiteMapArray();
        }
		if($isTopOriginal){
            $mapKeyInfoList = App_Model_List_Original::$EXTEND_INFO_LIST;
		    foreach($this->view->siteMapData as $k => &$v){
                if($v['page_type_code'] == App_Model_DbTable_HpPage::TYPE_INFO_INDEX){
                    $notiSetting = App_Model_DbTable_HpMainParts::slave()->getSettingForNotification(
                        $v['link_id'],
                        $hp->id
                    );
                    if(!$notiSetting) continue;
                    $notiSettingData = $notiSetting->toArray();
                    $key = $notiSettingData[$mapKeyInfoList['notification_type']];
                    $textList = App_Model_List_Original::getInfoPageName($key);
                    $v['text'] = $textList[$v['page_type_code']] ;
                    if(!(isset($v['detail']) && !is_null($v['detail']))) continue;
                    $v['detail']->text = $textList[$v['page_type_code'] + 1] ;
                }
                elseif($v['page_type_code'] == App_Model_DbTable_HpPage::TYPE_TOP){
                    $v['title'] = App_Model_List_Original::TOP_CONTENT;
                }
            }

            #3603
            #2019/02/18 don't hide special
//            $housingBlocksHidden = array();
//            $topPage = App_Model_DbTable_HpPage::slave()->getTopPageData($hp->id);
//            $housingBlocksData = $topPage->fetchParts(App_Model_DbTable_HpMainParts::PARTS_ESTATE_KOMA);
//            $housingBlocks= $housingBlocksData->toArray();
//            $display_flg = Custom_Hp_Page_Parts_EstateKoma::CMS_DISABLE;
//            $special_id = Custom_Hp_Page_Parts_EstateKoma::SPECIAL_ID_ATTR;
//            foreach($housingBlocks as $k => $housingBlock){
//                if($housingBlock[$display_flg] == 1) continue;
//                $housingBlocksHidden[] = $housingBlock[$special_id];
//            }
//            $this->view->housingBlocksHidden = $housingBlocksHidden;
        }
	}
	
	/**
	 * 新規ページを追加
	 */
	public function apiCreatePageAction() {
		$this->_helper->csrfToken();

		$form = new Default_Form_SiteMap_Page();
		if (!$form->isValid($this->getAllParams())) {
			$this->data->errors = $form->getMessages();
			return;
		}

        $hp = $this->getUser()->getCurrentHp();
        $company = $hp->fetchCompanyRow();

        $isTopOriginal = Custom_User_Abstract::getInstance()->checkAvailableTopOriginal($company->id);

		// 一意なタイプの確認
		$table = App_Model_DbTable_HpPage::master();
		$table->setPageTypeInfoUnique($hp,$isTopOriginal);

		$type = (int)$form->page_type_code->getValue();
		if (in_array($type, $table->getUniqueTypeList(), true)) {
			if ($table->fetchRow(array('page_type_code = ?'=>$type, 'hp_id = ?'=>$hp->id))) {
				$form->page_type_code->addError('ひとつのみ作成可能なページです。');
				$this->data->errors = $form->getMessages();
				return;
			}
		}
		
		// 会員ページチェック
		if ($type === App_Model_DbTable_HpPage::TYPE_MEMBERONLY && Custom_Util::isEmpty($form->parent_page_id->getValue())) {
			$form->page_type_code->addError('階層外には作成できない種別です。');
			$this->data->errors = $form->getMessages();
			return;
		}
		
		$items = array();
		
		$adapter = $table->getAdapter();
		$adapter->beginTransaction();
		
		$data = $form->getValues();
		$data['title']       = $table->getTypeNameJp($data['page_type_code']);
		$data['description'] = $table->getDescriptionNameJp($data['page_type_code']);
		$data['keywords']    = $table->getKeywordNameJp($data['page_type_code']);
		$data['filename']    = $table->getPageNameJp($data['page_type_code']);

		$data['new_flg'] = 1;
        $data = $this->_beforeSave($data, $form);
        
        $this->_updateSort($table, $data);
		
		$row = $table->createRow($data);
		// ATHOME_HP_DEV-2626 「会員さま専用ページ」が「未作成」だと配下のページが一般ページと判断される
		if ($type === App_Model_DbTable_HpPage::TYPE_MEMBERONLY )
		{
			$row->member_only_flg	= 1		;
		}
		$row->save();
		
		$row->link_id = $row->id;
		$row->save();
		
		$items[] = $row->toSiteMapArray();
		
		// 一覧ページ且つ、詳細まとめでない場合
		if (
			$table->hasDetailPageType($row->page_type_code) &&
			!$table->hasMultiPageType($row->page_type_code)
		) {
			// 子を作成する
			$childType = $row->page_type_code + 1;
			$childData = array(
					'title' => $table->getTypeNameJp($childType),
					'description' => $table->getDescriptionNameJp($childType),
					'keywords'    => $table->getKeywordNameJp($childType),
					'filename'    => $table->getPageNameJp($childType),
					'new_flg' => 1,
					'page_type_code' => $childType,
					'parent_page_id' => $row->id,
					'sort' => 0,
			);
			$childData = $this->_beforeSave($childData);
			$childData['level'] = $row->level + 1;
			$childRow = $table->createRow($childData);
			$childRow->save();
			
			$childRow->link_id = $childRow->id;
			$childRow->save();
			
			$items[] = $childRow->toSiteMapArray();
		}
		
		$adapter->commit();
		
		$this->data->items = $items;
	}
	
	
	/**
	 * 既存ページを追加
	 * 階層外からグロナビへ移動
	 */
	public function apiAddPageAction() {
		$this->_helper->csrfToken();
		
		$hp = $this->getUser()->getCurrentHp();
		
		$table = App_Model_DbTable_HpPage::master();
		$row = $table->fetchRow(array('id = ?' => (int)$this->getParam('id'), 'hp_id = ?' => $hp->id));
		if (!$row) {
			$this->data->errors = array('id' => array('ページが存在しません。'));
			return;
		}
		
		if ($row->parent_page_id !== null) {
			$this->data->errors = array('id' => array('階層内のページは追加できません。'));
			return;
		}
		
		$autoCreateIndex = false;
		
		$params = $this->getAllParams();
		$params['page_type_code'] = $row->page_type_code;
		
		$items = array();
		
		$form = new Default_Form_SiteMap_Page();
		if (!$form->isValid($params)) {
			// 詳細ページの場合は一覧ページで再度バリデーション
			if ($table->isDetailPageType($row->page_type_code)) {
				$autoCreateIndex = true;
				$params['page_type_code'] = $row->page_type_code - 1;
				$form = new Default_Form_SiteMap_Page();
				if (!$form->isValid($params)) {
					$this->data->errors = $form->getMessages();
					return;
				}
			}
			else {
				$this->data->errors = $form->getMessages();
				return;
			}
		}
		
		$adapter = $table->getAdapter();
		$adapter->beginTransaction();
		
		$data = $form->getValues();
        $data['diff_flg'] = 1;
        $data = $this->_beforeSave($data, $form);
        
        $this->_updateSort($table, $data);
		
		if ($autoCreateIndex) {
			// 一覧ページを作成
			$parentData = $data;
			$parentData['title'] = $table->getTypeNameJp($parentData['page_type_code']);
			$parentData['new_flg'] = 1;
			$parentRow = $table->createRow($parentData);
			$parentRow->save();
			
			$parentRow->link_id = $parentRow->id;
			$parentRow->save();
			
			$data = array(
					'parent_page_id' => $parentRow->id,
					'level' => $parentRow->level + 1,
					'sort' => 0,
                    'diff_flg' => 1,
			);
			
			$items[] = $parentRow->toSiteMapArray();
			
		}

		$row->setFromArray($data);
		$row->save();
		
		$items[] = $row->toSiteMapArray();
		
		$adapter->commit();
		
		$this->data->items = $items;
	}
	
	/**
	 * 既存ページへのリンクを追加
	 */
	public function apiCreateAliasAction() {
		$this->_helper->csrfToken();
		
		$hp = $this->getUser()->getCurrentHp();
		
		$params = $this->getAllParams();
		if (isset($params['link_page_id']) && strpos((string)$params['link_page_id'], 'estate_') === 0) {
			$params['page_type_code'] = App_Model_DbTable_HpPage::TYPE_ESTATE_ALIAS;
			$form = new Default_Form_SiteMap_EstateAlias();
			$isEstate = true;
		}
		else {
			$params['page_type_code'] = App_Model_DbTable_HpPage::TYPE_ALIAS;
			$form = new Default_Form_SiteMap_Alias();
			$isEstate = false;
		}
		if (!$form->isValid($params)) {
			$this->data->errors = $form->getMessages();
			return;
		}

		$table   = App_Model_DbTable_HpPage::master();
		$adapter = $table->getAdapter();
		try {
			$adapter->beginTransaction();
		
		    $data = $form->getValues();
            $data['diff_flg'] = 1;
		    $data = $this->_beforeSave($data, $form);
		
		    // 物件ページへのリンクの場合、link_estate_page_idに保存する
		    if ($isEstate) {
			    $data['link_estate_page_id'] = $data['link_page_id'];
			    unset($data['link_page_id']);
		    }
		
            $table = App_Model_DbTable_HpPage::master();
            $this->_updateSort($table, $data);
		    $row = $table->createRow($data);
		    $row->save();
		    $this->data->items = array($row->toSiteMapArray());
			
			$adapter->commit();
		} catch (Exception $e) {
			$adapter->rollback();
			throw $e;
		}
	}
	
	/**
	 * 既存ページへのリンクを編集
	 */
	public function apiUpdateAliasAction() {
		$this->_helper->csrfToken();
		
		$hp = $this->getUser()->getCurrentHp();
		
		$where = array(
				'id = ?'=>(int)$this->getParam('id'),
				'page_type_code in (?)'=>[
					App_Model_DbTable_HpPage::TYPE_ALIAS,
					App_Model_DbTable_HpPage::TYPE_ESTATE_ALIAS,],
				'hp_id' => $hp->id
		);
		
		$table = App_Model_DbTable_HpPage::master();
		$row = $table->fetchRow($where);
		if (!$row) {
			$this->_forward404();
		}
		
		$params = $this->getAllParams();
		$params['parent_page_id'] = $row->parent_page_id;
		$params['sort'] = $row->sort;
		if (isset($params['link_page_id']) && strpos((string)$params['link_page_id'], 'estate_') === 0) {
			$params['page_type_code'] = App_Model_DbTable_HpPage::TYPE_ESTATE_ALIAS;
			$form = new Default_Form_SiteMap_EstateAlias();
			$isEstate = true;
		}
		else {
			$params['page_type_code'] = App_Model_DbTable_HpPage::TYPE_ALIAS;
			$form = new Default_Form_SiteMap_Alias();
			$isEstate = false;
		}
		if (!$form->isValid($params)) {
			$this->data->errors = $form->getMessages();
			return;
		}


		$table   = App_Model_DbTable_HpPage::master();
		$adapter = $table->getAdapter();
		try {
			$adapter->beginTransaction();
		
		    $data = $form->getValues();
            $data['diff_flg'] = 1;
		    $data = $this->_beforeSave($data, $form);
		
    		// 物件ページへのリンクの場合、link_estate_page_idに保存する
		    if ($isEstate) {
			    $data['link_estate_page_id'] = $data['link_page_id'];
			    $data['link_page_id'] = null;
		    }
		    else {
			    $data['link_estate_page_id'] = null;
		    }
		
		    $row->setFromArray($data);
		    $row->save();
		    $this->data->items = array($row->toSiteMapArray());
			
			$adapter->commit();
		} catch (Exception $e) {
			$adapter->rollback();
			throw $e;
		}
	}
	
	/**
	 * URLリンクを追加
	 */
	public function apiCreateLinkAction() {
		$this->_helper->csrfToken();
		
		$hp = $this->getUser()->getCurrentHp();
		
		$params = $this->getAllParams();
		$params['page_type_code'] = App_Model_DbTable_HpPage::TYPE_LINK;
		$form = new Default_Form_SiteMap_Link();
		if (!$form->isValid($params)) {
			$this->data->errors = $form->getMessages();
			return;
		}

		$table   = App_Model_DbTable_HpPage::master();
		$adapter = $table->getAdapter();
		try {
			$adapter->beginTransaction();
		
		    $data = $form->getValues();
            $data['diff_flg'] = 1;
		    $data = $this->_beforeSave($data, $form);
		
            $table = App_Model_DbTable_HpPage::master();
            $this->_updateSort($table, $data);            
            $row = $table->createRow($data);
		    $row->save();
		    $this->data->items = array($row->toSiteMapArray());
			
			$adapter->commit();
		} catch (Exception $e) {
			$adapter->rollback();
			throw $e;
		}
		
	}
	
	/**
	 * URLリンクを編集
	 */
	public function apiUpdateLinkAction() {
		$this->_helper->csrfToken();
		
		$hp = $this->getUser()->getCurrentHp();
		
		$where = array(
				'id = ?'=>(int)$this->getParam('id'),
				'page_type_code = ?'=>App_Model_DbTable_HpPage::TYPE_LINK,
				'hp_id' => $hp->id
		);
		
		$table = App_Model_DbTable_HpPage::master();
		$row = $table->fetchRow($where);
		if (!$row) {
			$this->_forward404();
		}
		
		$params = $this->getAllParams();
		$params['page_type_code'] = App_Model_DbTable_HpPage::TYPE_LINK;
		$params['parent_page_id'] = $row->parent_page_id;
		$params['sort'] = $row->sort;
		$form = new Default_Form_SiteMap_Link();
		if (!$form->isValid($params)) {
			$this->data->errors = $form->getMessages();
			return;
		}

		$table   = App_Model_DbTable_HpPage::master();
		$adapter = $table->getAdapter();
		try {
			$adapter->beginTransaction();
		
		    $data = $form->getValues();
            $data['diff_flg'] = 1;
		    $data = $this->_beforeSave($data);
		
		    $row->setFromArray($data);
		    $row->save();
		    $this->data->items = array($row->toSiteMapArray());
			
			$adapter->commit();
		} catch (Exception $e) {
			$adapter->rollback();
			throw $e;
		}
		
	}
	
	protected function _beforeSave($data, $form = null) {
		if (Custom_Util::isEmptyKey($data, 'parent_page_id')) {
			$data['parent_page_id'] = null;
			$data['level'] = 1;
		}
		else {
			if ($data['parent_page_id'] == 0) {
				$data['level'] = 1;
			}
			else if ($form) {
				$validator = $form->parent_page_id->getValidator('Custom_Validate_ParentHpPageId');
				$data['level'] = $validator->getRow()->level + 1;
			}
		}
		
		// カテゴリを設定
		$table = App_Model_DbTable_HpPage::slave();
		$data['page_category_code'] = $table->getCategoryByType($data['page_type_code']);

		// ホームページID
		$data['hp_id'] = $this->getUser()->getCurrentHp()->id;
		
		return $data;
	}
	
	/**
	 * ページをメニューから削除
	 */
	public function apiRemoveFromMenuAction() {
		$this->_helper->csrfToken();
		
		$id = (int) $this->getParam('id');
		$hp = $this->getUser()->getCurrentHp();
		
		$table = App_Model_DbTable_HpPage::master();
		$row = $table->fetchRow(array('id = ?' => $id, 'hp_id = ?' => $hp->id));
		
		if (!$row) {
			$this->data->error = "ページが存在しません。";
			return;
		}
		
		// トップページはグロナビ固定
		if ($row->page_type_code == App_Model_DbTable_HpPage::TYPE_TOP || $row->page_type_code == App_Model_DbTable_HpPage::TYPE_MEMBERONLY) {
			$this->data->error = $table->getTypeNameJp($row->page_type_code)."は削除できません。";
			return;
		}
		
		// 詳細まとめは直接操作不可
		if ($table->isMultiPageType($row->page_type_code)) {
			$this->data->error = '削除できません。';
			return;
		}
		
		$items = array();
		
		// 階層外へ移動
		$adapter = $table->getAdapter();
		$adapter->beginTransaction();
		
		// 詳細の場合、兄弟がいない場合は親削除
		if ($table->isDetailPageType($row->page_type_code) && $row->parent_page_id) {
			$siblings = $table->fetchAll(array('id != ?'=>$row->id, 'parent_page_id = ?'=>$row->parent_page_id, 'hp_id = ?'=>$row->hp_id));
			if (!count($siblings)) {
				if ($parentRow = $table->fetchRow(array('id = ?'=>$row->parent_page_id, 'hp_id = ?'=>$row->hp_id))) {
					$table->setAutoLogicalDelete(false);
					$parentRow->delete_flg = 1;
					$parentRow->save();
					$table->setAutoLogicalDelete(true);
					$items[] = $parentRow->toSiteMapArray();
				}
			}
            // 兄弟がいる場合、親の差分フラグON
            else {
                if ($row->public_flg && $parentRow = $table->fetchRow(array('id = ?'=>$row->parent_page_id, 'hp_id = ?'=>$row->hp_id))) {
                    $parentRow->diff_flg = 1;
                    $parentRow->save();
                    $items[] = $parentRow->toSiteMapArray();
                }
            }
		}
		$items = array_merge($items, $row->removePageFromMenuRecursive());
		
		$adapter->commit();
		
		$this->data->items = $items;
	}
	
	/**
	 * 並び替え
	 */
	public function apiSortAction() {
		$this->_helper->csrfToken();
		
		$hp = $this->getUser()->getCurrentHp();
		
		$sort = $this->getParam('sort');
		if (!is_array($sort)) {
			return;
		}
		
		$items = array();
		$table = App_Model_DbTable_HpPage::master();
		foreach ($sort as $i => $id) {

            $row = $table->fetchRowById($id);
            // 並び順変更なければcontinue
            if ($row->sort == $i) {
                continue;
            }

			if (is_numeric($i) && is_numeric($id)) {
				$table->update(array('sort' => $i, 'diff_flg' => 1), array('id = ?'=>$id, 'hp_id = ?'=>$hp->id));
				$items[] = array(
						'id' => $id,
						'sort' => $i,
				);
				
			}
		}
		
		$this->data->items = $items;
    }
    
    protected function _updateSort($table, $data) {
        $where = array(
            'hp_id=?' => $data['hp_id'],
            'sort >= ?' => $data['sort'],
            'level = ?' => $data['level'],
            'delete_flg = 0',
        );
        if ($data['parent_page_id'] != null) {
            $where = array_merge($where, array('parent_page_id = ?' => $data['parent_page_id']));
        }
        $rows = $table->fetchAll($where, array('sort'));
        foreach ($rows as $row) {
            if($rows[0]->sort != $data['sort']) break;
            $row->sort = $row->sort + 1;
            $row->save();
        }
    }
}