(function (app) {
	'use strict';
	
	app.SiteMap = function () {
		this.token = null;
		this.$container = null;
	};
	app.SiteMap.prototype = {
		init: function (token, $container) {
			this.token = token;
			
			this.pages = {};
			this.estatePages = [];
			
			this.$container  = $container;
			
			var $editSitemap = $container.find('.edit-sitemap');
			this.global  = new app.SiteMap.MenuArea($editSitemap.eq(0).find('.sitemap-main > ul'));
			this.fixed   = new app.SiteMap.FixedArea($editSitemap.eq(0).find('.sitemap-fix > ul'));
			var $outSitemap = $editSitemap.eq(1).find('.sitemap-fix > ul');
			this.free    = new app.SiteMap.Area($outSitemap.eq(0));
			this.special = new app.SiteMap.Area($outSitemap.eq(1));
			
			var modal = app.SiteMap.addModal();
			var editinkModal = app.SiteMap.editLinkModal();
			var menuDragModal = app.SiteMap.menuDragModal();
			
			var self = this;
			
			// $container.on('drop','ul > .last',function (event){
			// 	this.style.border='';
			// 	clearAreaDrag();
			// 	var familyIds 	=	event.originalEvent.dataTransfer.getData("farmily-id");
			// 	var parentLast  =	this.parentNode;
			// 	if(	familyIds.indexOf(parentLast.parentNode.dataset.id) !=-1 ){
			// 		console.log('không được thực hiện hành động này');
			// 		return;
			// 	}
			// 	else{
			// 		event.preventDefault();
			// 		console.log('thực hiện đổi level');
			// 		//@todo
			// 		var sorts 		=	JSON.parse('['+event.originalEvent.dataTransfer.getData("sorts")+']');
			// 		var idDrag		=	parseInt(event.originalEvent.dataTransfer.getData("data-id"));
			// 		var levelDrag	= 	parseInt(event.originalEvent.dataTransfer.getData("level"));
			// 		var levelDrop	=	parseInt(this.parentNode.className.replace('level',''));
			// 		var thanLevel = levelDrag-levelDrop;
			// 		var elementDrag     =   $container.find('.app-sitemap-page-item[data-id="'+idDrag+'"]');
			// 		if(thanLevel>0)
			// 		{
			// 			var elementDrop = $(getElementLevelMore(thanLevel));
			// 			elementDrop.after(elementDrag);
			// 		}
			// 		else{
			// 			$(this).before(elementDrop);
			// 		}	

			// 	}
			// });

			// function getElementLevelMore(thanLevel){
				
			// 	$container
			// 	if(thanLevel==0){
			// 		return;
			// 	}
			// 	else{

			// 		getElementLevelMore(--thanLevel);
			// 	}
			// }

			// // $container.on('click', '.pull', function () {
            // //     var dataId = $(this).parent().parent().parent().attr('data-id');
            // //     menuDragModal.$el.find('.modal-sitemap').children().each(function () {
            // //         $(this).attr('data-id', dataId);
            // //     })
            // //     menuDragModal.show();
    		// // 	return false;
            // // });
            // var timeout; 
            // $('body').on('keyup', function (event) {
            //     if ($('.app-sitemap-page-item').hasClass('up-and-down')) {
            //         event.preventDefault();
            //         var keyDownEvent = event || window.event,
            //         keycode = (keyDownEvent.key) ? keyDownEvent.key : keyDownEvent.keyCode;
            //         var page = $('.up-and-down');
            //         if (keycode == 'ArrowUp' || keycode == 'Up') {
            //             var prev = page.prev('.app-sitemap-page-item');
            //             if (page.index() >= 1) {
            //                 switchItem(page, prev);
            //             }
            //         }
            //         if (keycode == 'ArrowDown' || keycode == 'Down') {
            //             var next = page.next('.app-sitemap-page-item');
            //             switchItem(next, page);
            //         }
            //         $('html, body').stop().animate({
            //             scrollTop: page.offset().top - 300
            //             },
            //             500
            //         );
            //         if (keycode == 'Escape' || keycode == 'Esc' || keycode == 'Enter') {
            //             page.removeClass('up-and-down');
            //             $(this).removeAttr('style');
            //             page.children().eq(1).css('box-shadow', 'unset');
            //         }
            //         if (timeout) {
            //             clearTimeout(timeout);
            //         }
            //         timeout = setTimeout(function () {
            //             updateSortDragAndUpDown(page);
            //         }, 2000); 
            //          return true;
            //     }
            //     return true;
            // });
			
			// $container.on('drop',".app-sitemap-page-item[data-type!='1'] > .item",function(event){
			// 	event.preventDefault();
			// 	this.style.border='';
			// 	var sorts  = JSON.parse('['+$('#darg-pannel-temp .item').eq(0)[0].dataset.sorts+']');
			// 	var idDrag = parseInt($('#darg-pannel-temp .item').eq(0)[0].dataset.dataId);
			// 	var idDrop	=	parseInt(this.parentNode.dataset.id);
			// 	var positionStart	=	sorts.indexOf(idDrag);
			// 	var positionEnd		=	sorts.indexOf(idDrop);
			// 	var elementDrag     =   $container.find('.app-sitemap-page-item[data-id="'+idDrag+'"]');
			// 	var elementDrop		=	$(this).parent();
			// 	if(sorts.indexOf(idDrop)!=-1){
			// 		if(positionStart>positionEnd){
			// 			elementDrop.before(elementDrag);
			// 		}
			// 		else if(positionStart<positionEnd){
			// 			elementDrop.after(elementDrag);
			// 		}
			// 		else{
			// 			return;
			// 		}
			// 		updateSortDragAndUpDown(elementDrag);
			// 		elementDrag.stop().animate({
			// 			opacity:1
			// 		},700,function(){
			// 			$(this).removeAttr('style');
			// 		});
			// 		var targetScrollDrag =$container.find('.app-sitemap-page-item[data-id="'+idDrag+'"]').offset().top - 200;
			// 		$('#darg-pannel-temp').remove();
			// 		$('body,html').stop().animate({
			// 	        scrollTop:  targetScrollDrag
			// 	    }, 500);
			// 	}
				
			// }) 

			// $container.on('click','.close-darg',function(){
			// 	var dataId=this.parentNode.dataset.id
			// 	$('.app-sitemap-page-item[data-id='+dataId+']').removeAttr('style');
			// 	this.parentNode.remove();
			// });

			// $container.on('dragover','.app-sitemap-page-item > .item',function(event){
			// 	event.preventDefault();
			// 	if(!$(this).hasClass('dragstart')){
			// 		this.style.border="1px solid blue";
			// 	}
			// })
			
			// $container.on('dragleave','.app-sitemap-page-item > .item',function(event){
			// 	if(!$(this).hasClass('dragstart')){
			// 		this.style.border='';
			// 	}
			// }) 

			// $container.on('dragstart','.item[draggable=true]',function(event){
			// 	//  init array sorts  .
			// 	var dataId=this.parentNode.dataset.id;
			// 	var sorts = [];
			// 	$('.app-sitemap-page-item[data-id='+dataId+']').parent().children().each(function (){
			// 		if($(this)[0].hasAttribute('data-id')){
			// 			sorts.push($(this).attr('data-id'));
			// 		}
			// 	});
			// 	this.dataset.dataId = dataId;
			// 	this.dataset.sorts = sorts;
			// 	if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
			// 		//run firefox draggale;
			// 	    event.originalEvent.dataTransfer.setData('runDrag','firefox');
			// 	}
			// 	if (typeof event.originalEvent.dataTransfer.setDragImage === 'function')
			// 	{
			// 		event.originalEvent.dataTransfer.setDragImage(this,50,50);
			// 	}
			// 	$(this).addClass('dragstart');
			// 	// Tick key current and render Areas Drag.
			// 	$(this.parentNode.parentNode).addClass('current');
			// })
			

			

			// 追加
			$container.on('click', '.item.add a', function (e, param) {
				var $this = $(this).parent().parent();
				var $parent = $this.parent().parent();
				var parent = null;
				
				// グロナビ
				if ($parent.hasClass('sitemap-main')) {
                    console.log('5');
					modal.setTypes(app.SiteMap.GlobalMenuTypes, self.global);
				}
				// グロナビ階層内
				else if ($parent.attr('data-id')) {
                    console.log('4');
					parent = self.getPage($parent.attr('data-id'));
					modal.setTypes(parent.getChildTypes(), parent);
				}
				// 階層外
				else {
                    console.log('3');
					modal.setTypes(app.SiteMap.NotInMenuTypes, self.free);
                }
                
                if(param){
                    console.log('1');
                    modal.$el.find('.modal-sitemap').attr('data-id', param);
                }else{
                    console.log('2');
                    modal.$el.find('.modal-sitemap').removeAttr('data-id');
                }
				
				modal.show();
				return false;
			});
			
			// 編集
			$container.on('click', '.app-sitemap-page-item-edit', function () {
				var $page = $(this).closest('.app-sitemap-page-item');
				var page;
				if (self.isEstatePageElement($page)) {
					page = self.getEstatePage($page.attr('data-type'), $page.attr('data-id'));
					if (page && page.getEditPageUrl()) {
						location.href = page.getEditPageUrl();
					}
					return false;
				}
				
				page = self.getPage($page.attr('data-id'));
				if (!page) {
					return false;
				}
				
				if (!page.isLink()) {
					location.href = '/page/edit?id=' + page.getId();
					return false;
				}
				
				editinkModal.setLink(page);
				editinkModal.show();
				
				return false;
			});
			
			// メニューから削除
			$container.on('click', '.app-sitemap-page-item-remove', function () {
				var $page = $(this).parents('.app-sitemap-page-item').eq(0);
				var page = self.getPage($page.attr('data-id'));
				if (page) {
					app.api('/site-map/api-remove-from-menu', {id: page.getId(), _token: self.token}, function (res) {
						if (res.error) {
							app.modal.alert('', '削除に失敗しました。');
							return;
						}
						
						self.setData(res.items);
					});
				}
				
				return false;
			});
			
			// 並び替え
			function switchItem($a, $b) {
				$a.after($b);
				
				if ($a.hasClass('global') !== $b.hasClass('global')) {
					$a.toggleClass('global');
					$b.toggleClass('global');
				}
			}

			function updateSortDragAndUpDown($elementDrag){
				var data = [];
				$elementDrag.parent().children().each(function (){
					data.push($(this).attr('data-id'));
				});
				app.api('/site-map/api-sort', {_token: self.token, sort: data}, function (res) {
					self.setData(res.items);
				}).fail(function () {
					switchItem($b, $a);
				});
			}

			function updateSort($a, $b) {
				
				switchItem($a, $b);
				
				var data = [];
				$a.parent().children().each(function (){
					data.push($(this).attr('data-id'));
				});
				
				app.api('/site-map/api-sort', {_token: self.token, sort: data}, function (res) {
					self.setData(res.items);
				}).fail(function () {
					switchItem($b, $a);
				});
            }
            
            $container.on('click', '.app-sitemap-page-item-new-page', function () {
                var $page = $(this).parents('.app-sitemap-page-item').eq(0);
				var $parent = $(this).parents('.app-sitemap-page-item').parent();
                $parent.children().last().find('.item.add a').trigger('click', [$page.attr('data-id')]);
            });
            /* 3014 remove: not use
			$container.on('click', '.app-sitemap-page-item-up', function () {
				if ($(this).hasClass('is-disable')) {
					return false
				}
				
				var $page = $(this).parents('.app-sitemap-page-item').eq(0);
				var $prev = $page.prev('.app-sitemap-page-item');
				if (app.SiteMap.IsTopOriginal && !app.SiteMap.IsAgency) {
					if ($prev.hasClass('global')) {
						return false;
					}
				}
				if (!$prev.length) {
					return false;
				}
				
				updateSort($page, $prev);
				
				return false;
			});
			$container.on('click', '.app-sitemap-page-item-down', function () {
				var $page = $(this).parents('.app-sitemap-page-item').eq(0);
				var $next = $page.next('.app-sitemap-page-item');
				if (!$next.length) {
					return false;
				}
				
				updateSort($next, $page);
				
				return false;
            });*/
            
            $container.on('click', '.app-sitemap-page-item-move', function () {
                var editMode = app.SiteMap.editMode();
                editMode.show();
                return false;
            });
            
			
			return this;
        },
		clear: function () {
			this.pages = {};
			this.estatePages = [];
			
			this.global.empty();
			this.fixed.empty();
			this.free.empty();
		},
		
		setData: function (data, elem) {
			var i;
			for (i in data) {
				if (data[i].deleted) {
					if (this.pages[data[i].id]) {
						this.pages[data[i].id].remove();
						delete this.pages[data[i].id];
					}
				}
				else {
					if (this.pages[data[i].id]) {
						this.pages[data[i].id].updateData(data[i]);
					}
					else {
						this.pages[data[i].id] = new app.SiteMap.Page(data[i]);
					}
				}
			}
			var page;
			var addedArea = {};
			for (i in data) {
				page = this.pages[data[i].id];
				if (page) {
					page.render();
					if (page.data.parent_page_id === 0) {
                        if (typeof elem == 'undefined') {
                            this.global.addChild(page);
                        } else {
                            this.global.addChild(page, elem);
                        }
						addedArea.global = true;
					}
					else if (page.data.parent_page_id !== null && this.pages[page.data.parent_page_id]) {
                        if (typeof elem == 'undefined') {
                            this.pages[page.data.parent_page_id].addChild(page);
                        } else {
                            this.pages[page.data.parent_page_id].addChild(page, elem);
                        }
						
					}
					else if (page.isFixedType()) {
						this.fixed.addChild(page);
						addedArea.fixed = true;
					}
					else {
						this.free.addChild(page);
						addedArea.free = true;
					}
				}
			}
			
			var self = this;
			$.each(addedArea, function (area) {
				self[area].sortChildren();
			});
			if (!addedArea.global) {
				this.global.updateGlobal();
			}
			return this;
		},
		
		setEstateData: function (data) {
			this.estatePages.top = new app.SiteMap.EstatePage(data.top);
			this.estatePages.chintai_top = new app.SiteMap.EstatePage(data.chintai_top);
			this.estatePages.baibai_top = new app.SiteMap.EstatePage(data.baibai_top);
			this.estatePages.push(this.estatePages.top);

			this.estatePages.estateTypes = [];
			var i,l,estateType, estateTypePage;
			for (i=0,l=data.estateTypes.length;i<l;i++) {
				estateType = data.estateTypes[i];
				if (estateType.estate_class == 1 || estateType.estate_class == 2) {
					if (!this.estatePages['chintai_top_pushed']) {
						this.estatePages.push(this.estatePages.chintai_top);
						this.estatePages['chintai_top_pushed'] = true;
					}
				}else if (estateType.estate_class == 3 || estateType.estate_class == 4) {
					if (!this.estatePages['baibai_top_pushed']) {
						this.estatePages.push(this.estatePages.baibai_top);
						this.estatePages['baibai_top_pushed'] = true;
					}
				}
				estateTypePage = new app.SiteMap.EstatePage(estateType);
				this.estatePages.top.addChild(estateTypePage);
				this.estatePages.estateTypes.push(estateTypePage);
				this.estatePages.push(estateTypePage);
			}
			this.estatePages.top.render(true);
			this.estatePages.chintai_top.render(true);
			this.estatePages.baibai_top.render(true);
			this.fixed.addChild(this.estatePages.top);
			if (this.estatePages['chintai_top_pushed'] == true) {
				this.fixed.addChild(this.estatePages.chintai_top);
			}
			if (this.estatePages['baibai_top_pushed'] == true) {
				this.fixed.addChild(this.estatePages.baibai_top);
			}
			this.fixed.sortChildren();
			
			var special, specialPage;
			this.estatePages.specials = [];
			// 2019/02/18 don't hide special anymore
			// var isTopOriginal = app.SiteMap.IsTopOriginal;
			// var housingBlocksHidden = [];
			// if(isTopOriginal){
			// 	housingBlocksHidden = app.SiteMap.housingBlocksHidden;
			// }
			for (i=0,l=data.specials.length;i<l;i++) {
				special = data.specials[i];
				// if(isTopOriginal){
				//   var special_id = special.origin_id.toString();
				//   // hidden
				//   if(housingBlocksHidden.indexOf(special_id) >= 0 )
				//   {
				// 	continue;
				//   }
				// }
				specialPage = new app.SiteMap.EstatePage(special);
				specialPage.render();
				this.estatePages.specials.push(specialPage);
				this.estatePages.push(specialPage);
				this.special.addChild(specialPage);
			}
		},
		
		getPage: function (id) {
			return this.pages[id];
		},
		
		getPageByLinkId: function (linkId) {
			for (var i in this.pages) {
				if (this.pages[i].getLinkId() === linkId) {
					return this.pages[i];
				}
			}
			return;
		},
		
		getEstatePage: function (type, id) {
			if (type === 'estate_top') {
				return this.estatePages.top;
			}
			else if (type === 'estate_rent') {
				return this.estatePages.chintai_top;
			}
			else if (type === 'estate_purchase') {
				return this.estatePages.baibai_top;
			}
			else if (type === 'estate_type') {
				return this.getEstateTypePage(id);
			}
			else if (type === 'estate_special') {
				return this.getEstateSpecialPage(id);
			}
			return null;
		},
		getEstateTypePage: function (id) {
			if (!this.estatePages.estateTypes) {
				return null;
			}
			for (var i=0,l=this.estatePages.estateTypes.length;i<l;i++) {
				if (this.estatePages.estateTypes[i].getId() == id) {
					return this.estatePages.estateTypes[i];
				}
			}
			return null;
		},
		getEstateSpecialPage: function (id) {
			if (!this.estatePages.specials) {
				return null;
			}
			for (var i=0,l=this.estatePages.specials.length;i<l;i++) {
				if (this.estatePages.specials[i].getId() == id) {
					return this.estatePages.specials[i];
				}
			}
			return null;
		},
		getEstatePages: function () {
			return this.estatePages;
		},
		isEstatePageElement: function ($el) {
			return $el.hasClass('app-sitemap-estate-page-item');
		},
		getPageByEstateLinkId: function (estate_page_id) {
			for (var i=0,l=this.estatePages.length;i<l;i++) {
				if (this.estatePages[i].getLinkId() === estate_page_id) {
					return this.estatePages[i];
				}
			}
			return null;
		},
		
		getPages: function () {
			return this.pages;
		},
		
		getAllPages: function () {
			var result = [];
			$.each(this.pages, function (i,page) {
				result.push(page);
			});
			$.each(this.estatePages, function (i, page) {
				result.push(page);
			});
			return result;
		},
		
		hasType: function (type) {
			return !!this.$container.find('.app-sitemap-page-item[data-type="'+type+'"]').length;
		}
	};
	app.SiteMap.instance = null;
	app.SiteMap.getInstance = function () {
		if (!app.SiteMap.instance) {
			app.SiteMap.instance = new app.SiteMap();
		}
		return app.SiteMap.instance;
	};
	
	app.SiteMap.Types = {};
	app.SiteMap.TypeNames = {};
	app.SiteMap.Categories = {};
	app.SiteMap.CategoryNames = {};
	
	app.SiteMap.TypeNamesForSelect = {};
	
	app.SiteMap.getTypeName = function (type) {
		return app.SiteMap.TypeNames[ type ] || '';
	};
	
	app.SiteMap.getTypeNameForSelect = function (type) {
		if (type === app.SiteMap.Types.TYPE_SHOP_INDEX || type === app.SiteMap.Types.TYPE_SHOP_DETAIL) {
			return '店舗案内';
		}
		return app.SiteMap.getTypeName(type).replace(/一覧|詳細/, '');
	};
	
	app.SiteMap.getCategoryName = function (category) {
		return app.SiteMap.CategoryNames[ category ] || '';
	};
	
	app.SiteMap.FixedMenuTypes = [];
	app.SiteMap.GlobalMenuTypes = [];
	app.SiteMap.NotInMenuTypes = [];
	app.SiteMap.UniqueTypes = [];
	
	app.SiteMap.HasDetailPageTypes = [];
	app.SiteMap.DetailPageTypes = [];
	app.SiteMap.HasMultiPageTypes = [];
	app.SiteMap.ChildTypes = {};
	app.SiteMap.CreateableCategoryList = {};
	app.SiteMap.CategoryMap = {};
	
	app.SiteMap.getCategoryFromType = function (type) {
		for (var category in app.SiteMap.CategoryMap) {
			if (app.arrayIndexOf(type, app.SiteMap.CategoryMap[category]) >= 0) {
				return parseInt(category);
			}
		}
		return null;
	};
	
	app.SiteMap.isUniqueType = function (type) {
		return app.arrayIndexOf(type, app.SiteMap.UniqueTypes) > -1;
	};
	
	app.SiteMap.isFixedType = function (type) {
		return app.arrayIndexOf(type, app.SiteMap.FixedMenuTypes) > -1;
	};
	
	app.SiteMap.hasDetailPageType = function (type) {
		return app.arrayIndexOf(type, app.SiteMap.HasDetailPageTypes) > -1;
	};
	
	app.SiteMap.isDetailPageType = function (type) {
		return app.arrayIndexOf(type, app.SiteMap.DetailPageTypes) > -1;
	};
	
	app.SiteMap.hasMultiPageType = function (type) {
		return app.arrayIndexOf(type, app.SiteMap.HasMultiPageTypes) > -1;
	};
	
	app.SiteMap.init = function (token, $container) {
		return app.SiteMap.getInstance().init(token, $container);
	};
	
    app.SiteMap.resetOptionDragModal = function () {
        $('.app-sitemap-page-item').removeClass('up-and-down');
        $('.app-sitemap-page-item > .item').css('box-shadow', 'unset');
        $('.app-sitemap-page-item').removeAttr('style');
        $('.sitemap-main').find('#darg-pannel-temp').remove();
        $('body').removeAttr('style');
    }
	
	app.SiteMap.menuDragModal = function () {
        var contents = '<div class="modal-sitemap">'+
                            '<div class = "drag-drop" style="font-size: 20px;padding: 15px;">'+
                                '<a href="javascript:;">ドラッグ＆ドロップ動作'+
                                    '<a class="tooltip" href="javascript:;">'+
                                        '<i class="i-s-tooltip"></i>'+
                                        '<div class="tooltip-body">'+
                                            '<p>- このオプションを選択すると、選択されたページが一時的なエリアに移転される'+'</br>'+
                                                '- エリアが選択出来たら、一時的なエリアにあるページをひきずって、手放す</p>'+
                                        '</div>'+
                                    '</a>'+
                                '</a>'+
                            '</div>'+
                            '<div class = "button-up-and-down" style="font-size: 20px;padding: 15px;">'+
                                '<a href="javascript:;">キーボードでの上下動作'+
                                    '<a class="tooltip" href="javascript:;">'+
                                        '<i class="i-s-tooltip"></i>'+
                                        '<div class="tooltip-body">'+
                                            '<p>このオプションを選択すると、選択されたページがアクティブされ、キーボードのキーでページを移転する'+'</br>'+
                                            '- キー<i class="i-e-up" style="display: inline-block"></i>でページを上に移転する.</br>'+
                                            '- キー<i class="i-e-down" style="display: inline-block"></i>でページを下に移転する.</br>'+
                                            '- ESC或いはENTERでこのプロセスを 終了させる.'
                                            '</p>'+
                                        '</div>'+
                                    '</a>'+
                                '</a>'+
                            '</div>'+
                        '</div>';
        var modal = app.modal.popup({
            title: 'ページ位置の変更メニュー',
            contents: contents,
            modalBodyInnerClass: 'align-top',
            autoRemove: false
        });
        modal.$el.find('.button-up-and-down').on('click', function () {
            app.SiteMap.resetOptionDragModal();
            var dataId = $(this).attr('data-id');
            $('body').css('overflow', 'hidden');
            $('.app-sitemap-page-item[data-id=' + dataId + '] > .item').css('box-shadow', '0px 0px 2px 2px green');
            $('.app-sitemap-page-item[data-id=' + dataId + ']').addClass('up-and-down');
            modal.close();
        })
        modal.$el.find('.drag-drop').on('click', function (event) {
            app.SiteMap.resetOptionDragModal();
        	var dataId = this.dataset.id;
        	var page_item = $('.app-sitemap-page-item[data-id=' + dataId + ']');
        	var positionLeft = page_item.offset().left + 260;
        	var positionLeftout  = page_item.offset().left - 233;
        	var widthWindow  = $(window).width();
            var popupPosition = (widthWindow > (positionLeft+250)? positionLeft:positionLeftout)+'px' ;
            var itemHeight = page_item.find('> .item').height();
        	page_item.css({'opacity':'.3','background':'#ddd'});
        	$('.sitemap-main').append('<div class="darg-pannel-temp" id="darg-pannel-temp" data-id="'+dataId+'">'+
                                            '<a class="tooltip" href="javascript:;" style="position: absolute;left: 5px;">'+
                                                '<i class="i-s-tooltip"></i>'+
                                                '<div class="tooltip-body">'+
                                                '<p>- こちらは選択されたページを納める一時的なエリア.'+'</br>'+
                                                '- この一時的なエリアからページを引きずって移転させたいエリアまで手放す.'+
                                                '</p>'+
                                                '</div>'+
                                            '</a>'+
                                            '<a class="close-darg" style="position:absolute;right:5px;top:5px"><i class="i-e-delete"></i></a>'+
                                        '</div>'
                                    );
        	$('#darg-pannel-temp').css({
        		'position': 'fixed',
			    'left': popupPosition,
			    'top': '100px',
			    'background': 'rgb(255, 255, 255)',
			    'width': '250px',
			    'height': (itemHeight + 44) + 'px',
			    'border': '2px dashed rgb(136, 136, 136)'
        	})
        	.append(page_item.find('> .item').eq(0)[0].outerHTML)
        	.find('.item').css({
        		'left':'50%',
        		'top':'calc(50% + 10px)',
        		'transform':'translate(-50%,-50%)',
        		'margin':0,
        		'overflow':'hidden'
        	})
        	.attr('draggable','true')
        	.find('.action').remove();

    //     	var dataId=this.parentNode.dataset.id;
				// var sorts = [];
				// $('.app-sitemap-page-item[data-id='+dataId+']').parent().children().each(function (){
				// 	if($(this)[0].hasAttribute('data-id')){
				// 		sorts.push($(this).attr('data-id'));
				// 	}
				// });
				// this.dataset.dataId = dataId;
				// this.dataset.sorts = sorts;
				// if (typeof event.originalEvent.dataTransfer.setDragImage === 'function')
				// {
				// 	event.originalEvent.dataTransfer.setDragImage(this,50,50);
				// }
				// $(this).addClass('dragstart');
				// // Tick key current and render Areas Drag.
				// $(this.parentNode.parentNode).addClass('current');
        	
        	modal.close();
        });
        modal.$el.find('.modal-btns .btn-t-blue').remove();
        return modal;
    }

    app.SiteMap.editMode = function () {
		var contents = '<div class="modal-sitemap">' +
							'<dl>' +
								'<dt>既存ページから選ぶ Millions of football fans in the country are googling Vietnam and Syria as the two countries prepare to face off in an Asian Games quarterfinal men’s football match in a few hours.</dt>' +
							'</dl>' +
							'<dl>' +
								'<dt><label><input type="checkbox" name="remember_check" value="1"> 今後、このメッセージを表示しない</label></dt>' +
							'</dl>' +
						'</div>';
		var modal = app.modal.popup({
			contents: contents,
			modalBodyInnerClass: 'align-top',
			autoRemove: false,
		});
		$(this).find('.modal-body-inner').css('height', 'auto');

		return modal;
	};


	app.SiteMap.addModal = function () {
		var contents = '<div class="modal-sitemap js-scroll-container" data-scroll-container-max-height="500" style="overflow-y:auto;">' +
							'<dl class="modal-sitemap-new">' +
								'<dt><label><input type="radio" checked="checked" name="radio-addpage" value="create">新規のページを追加</label></dt>' +
								'<dd>' +
									'<div class="category-select">' +
										'<label>カテゴリ</label>' +
										'<select></select>' +
										'<div class="errors"></div>' +
									'</div>' +
									'<div class="type-select">' +
										'<label>ページ</label>' +
										'<select id="page_type_code"></select>' +
										'<div class="errors"></div>' +
									'</div>' +
								'</dd>' +
							'</dl>' +
							'<dl class="modal-sitemap-edit">' +
								'<dt><label><input type="radio" name="radio-addpage" value="add">既存ページを追加</label></dt>' +
								'<dd>' +
									'<div>' +
										'<select id="id"></select>' +
										'<div class="errors"></div>' +
									'</div>' +
								'</dd>' +
							'</dl>' +
							'<dl class="modal-sitemap-link">' +
								'<dt><label><input type="radio" name="radio-addpage" value="link">リンクを追加</label></dt>' +
								'<dd>' +
									'<div class="app-sitemap-link-module">' +
										'<label><input type="radio" name="radio-addpage-link">既存ページから選ぶ</label>' +
										'<select id="link_page_id"></select>' +
										'<div class="errors"></div>' +
									'</div>' +
									'<div class="app-sitemap-link-module">' +
										'<label><input type="radio" name="radio-addpage-link">URLからリンクを作る</label>' +
										'<ul>' +
											'<li>' +
												'<label>URL</label>' + 
												'<input type="text" id="link_url" name="link_url" maxlength="255" class="watch-input-count"><span class="input-count">0/255</span>' +
												'<div class="errors"></div>' +
											'</li>' +
											'<li>' +
												'<label>リンク名</label>' + 
												'<input type="text" id="title" name="title" maxlength="20" class="watch-input-count"><span class="input-count">0/30</span>' +
												'<div class="errors"></div>' +
											'</li>' +
											'<li>' +
												'<label><input type="checkbox" name="link_target_blank" value="1">別窓で開く</label>' + 
											'</li>' +
										'</ul>' +
									'</div>' +
								'</dd>' +
							'</dl>' +
						'</div>';
		var modal = app.modal.popup({
			title: 'ページまたはリンクの追加',
			contents: contents,
			modalBodyInnerClass: 'align-top',
			ok: '追加',
			autoRemove: false
		});
		
		// タイトル
		var $title = modal.$el.find('.modal-header h2');
		
		// 入力変更した場所を選択する
		modal.$el.find('.modal-sitemap dd').find('input:not([type="checkbox"]),select').on('change', function () {
			$(this).parents('dl').find('dt input').prop('checked', true);

			var $link = $(this).parents('.app-sitemap-link-module');
			if ($link.length) {
				$link.find('input[type="radio"]').prop('checked', true);
			}
		});
		
		// 新規ページ
		var $newPageModule         = modal.$el.find('.modal-sitemap-new');
		var $newPageCategoryModule = $newPageModule.find('.category-select');
		var $newPageCategorySelect = $newPageCategoryModule.find('select');
		var $newPageTypeModule     = $newPageModule.find('.type-select');
		var $newPageTypeSelect     = $newPageTypeModule.find('select');
		
		// 既存ページ
		var $editPageModule = modal.$el.find('.modal-sitemap-edit');
		var $editPageSelect = $editPageModule.find('select');
		
		// リンク
		var $linkModule = modal.$el.find('.modal-sitemap-link');
		var $linkPageModule = $linkModule.find('> dd div.app-sitemap-link-module:eq(0)');
		var $linkPageSelect = $linkPageModule.find('select');
		var $linkUrlModule  = $linkModule.find('> dd div.app-sitemap-link-module:eq(1)');
		var $linkUrlValue   = $linkUrlModule.find('input[name="link_url"]');
		var $linkUrlTitle   = $linkUrlModule.find('input[name="title"]');
		var $linkUrlTargetBlank = $linkUrlModule.find('input[name="link_target_blank"]');
		
		var $errors = modal.$el.find('.errors');
		modal.clearErrors = function () {
			$errors.empty();
			modal.$el.find('.is-error').removeClass('is-error');
		};
		
		modal.pageTypes = {};

    $newPageModule.on('change', '.category-select select', function () {
			var category = $(this).val();
			var types = modal.pageTypes[category] || [];

			$newPageTypeSelect.empty();
			$.each(types, function (i, type) {
				$newPageTypeSelect.append('<option value="'+ type +'">'+ app.SiteMap.getTypeNameForSelect(type) +'</option>');
			});
		});
		
		modal.$el.find('input:radio').on('change', function () {
			var isDisabled;
			
			isDisabled = !$newPageModule.find('input[name="radio-addpage"]').prop('checked');

      $newPageCategoryModule.find('label').after( $newPageCategorySelect.remove().prop('disabled', isDisabled) );
      $newPageTypeSelect.prop('disabled', isDisabled);

			$newPageModule.find('label').toggleClass('is-disable', isDisabled);
			$newPageModule.find('select').toggleClass('is-disable', isDisabled);
			
			isDisabled = !$editPageModule.find('input[name="radio-addpage"]').prop('checked');
			$editPageModule.find('select').prop('disabled', isDisabled);
			$editPageModule.find('select').toggleClass('is-disable', isDisabled);
			$editPageModule.find('label').toggleClass('is-disable', isDisabled);
			
			isDisabled = !$linkModule.find('input[name="radio-addpage"]').prop('checked');
			$linkModule.find('dd input:radio,input:checkbox').prop('disabled', isDisabled);
			$linkModule.find('label').toggleClass('is-disable', isDisabled);
			
			isDisabled = !$linkModule.find('input[name="radio-addpage"]').prop('checked') ||
							!$linkPageModule.find('input:radio').prop('checked');
			$linkPageModule.find('select').prop('disabled', isDisabled);
			$linkPageModule.find('select,div').toggleClass('is-disable', isDisabled);
			
			isDisabled = !$linkModule.find('input[name="radio-addpage"]').prop('checked') ||
							!$linkUrlModule.find('input:radio').prop('checked');
			$linkUrlModule.find('input:text').prop('disabled', isDisabled);
			$linkUrlModule.find('li:lt(2)').toggleClass('is-disable', isDisabled);
			
		});
		
		/**
		 * 作成可能タイプ設定
		 * @param {Array} types
		 * @param {Boolean} inMenu
		 * @param {app.SiteMap.Page=} parent
		 */
		modal.setTypes = function (types, parent) {
			modal.clearErrors();
			
			var sitemap = app.SiteMap.getInstance();
			
			modal.types = types;
			modal.parent = parent;
			
			// 新規ページ設定
			var pageTypes = modal.pageTypes = {};
			$.each(types, function (i, type) {
				// 存在する一意ページ除外
				if (app.SiteMap.isUniqueType(type) && app.SiteMap.getInstance().hasType(type)) {
					return;
				}
				
				// リンク除外
				if (type === app.SiteMap.Types.TYPE_LINK || type == app.SiteMap.Types.TYPE_ALIAS || type == app.SiteMap.Types.TYPE_ESTATE_ALIAS) {
					return;
				}
				
				var category = app.SiteMap.getCategoryFromType(type);
				if (category) {
					if (!pageTypes[category]) {
						pageTypes[category] = [];
					}
					pageTypes[category].push(type);
				}
			});
			
			$newPageCategorySelect.empty();
			$.each(pageTypes, function (category) {
				$newPageCategorySelect.append('<option value="'+ category +'">'+ app.SiteMap.getCategoryName(category) +'</option>');
			});
			$newPageCategorySelect[0].selectedIndex = 0;
			$newPageCategorySelect.change();
			$newPageCategoryModule.toggleClass('is-hide', !$newPageCategorySelect.children().length);
			$newPageTypeModule.toggleClass('is-hide', !$newPageCategorySelect.children().length);
			$newPageModule.toggleClass('is-hide', $newPageCategoryModule.hasClass('is-hide') && $newPageTypeModule.hasClass('is-hide'));
			
			// 既存ページ設定
			$editPageSelect.empty();
			if (parent.inMenu()) {
				$.each(sitemap.free.getChildren(), function (i, page) {
					// 作成可能なページタイプor詳細ページの場合は親タイプが作成可能な場合
					if (
						(app.arrayIndexOf(page.getType(), types) > -1) ||
						(page.isDetailPageType() && app.arrayIndexOf(page.getParentType(), types) > -1)
					) {
						$editPageSelect.append($('<option/>').attr('value', page.getId()).text(page.getTitleWithFilename()));
					}
				});
			}
			$editPageSelect[0].selectedIndex = 0;
			$editPageModule.toggleClass('is-hide', !$editPageSelect.children().length);
			
			// リンク設定
			if (app.arrayIndexOf(app.SiteMap.Types.TYPE_ALIAS, types) > -1) {
				// エイリアスが作成可能な場合
				$linkPageModule.removeClass('is-hide');
				$linkPageSelect.empty();
				$.each(sitemap.getAllPages(), function (id, page) {
					if (page.canAlias()) {
						// リンク以外を選択肢に追加
						$linkPageSelect.append($('<option/>').attr('value', page.getLinkId()).text(page.getTitleWithFilename()));
					}
				});
			}
			else {
				$linkPageModule.addClass('is-hide');
			}
			
			if (app.arrayIndexOf(app.SiteMap.Types.TYPE_LINK, types) > -1) {
				// URLリンクが作成可能な場合
				$linkUrlModule.removeClass('is-hide');
				$linkUrlValue.val('').change();
				$linkUrlTitle.val('').change();
				$linkUrlTargetBlank.prop('checked', false);
			}
			else {
				$linkUrlModule.addClass('is-hide');
			}
			$linkModule.toggleClass('is-hide', $linkPageModule.hasClass('is-hide') && $linkUrlModule.hasClass('is-hide'));
			
			// タイトルと各モジュール初期選択
			var title = '';
			if ((parent instanceof app.SiteMap.Page) && parent.hasDetailPageType()) {
				title = app.SiteMap.getTypeNameForSelect(parent.getType());
				
				// 一覧ページの場合、ページ選択セレクトボックスを非表示
				$newPageCategoryModule.addClass('is-hide');
				$newPageTypeModule.addClass('is-hide');
			}
			else {
				title = 'ページ';
			}
			
			if (!$linkModule.hasClass('is-hide')) {
				// リンクが作成可能な場合タイトルに追加
				title += 'またはリンク';
			}
			
			title += 'の追加';
			$title.text(title);
			
			if (!$newPageModule.hasClass('is-hide')) {
				$newPageModule.find('input[type="radio"]').eq(0).prop('checked', true);
			}
			else if (!$editPageModule.hasClass('is-hide')) {
				$editPageModule.find('input[type="radio"]').eq(0).prop('checked', true);
			}
			else if (!$linkModule.hasClass('is-hide')) {
				$linkModule.find('input[type="radio"]').eq(0).prop('checked', true);
			}
			
			$linkPageModule.find('input[type="radio"]').prop('checked', !$linkPageModule.hasClass('is-hide'));
			$linkUrlModule.find('input[type="radio"]').prop('checked', $linkPageModule.hasClass('is-hide'));

			modal.$el.find('input:radio:eq(0)').change();
		};
		
		// 閉じる処理
		modal.onClose = function (ret, modal) {
			if (!ret) {
				return;
			}
			
			var sitemap = app.SiteMap.getInstance();
			
			var addType = modal.$el.find('input[name="radio-addpage"]:checked').val();
			var url;
			var data = {};
			
			// 共通パラメータ
			data._token = sitemap.token;
			data.parent_page_id = modal.parent.getId();
            data.sort = modal.parent.newSortNumber();
            var dataId = modal.$el.find('.modal-sitemap').attr('data-id');
            if(!dataId){
                data.sort = modal.parent.newSortNumber();
            }else{
                var elem = $('.app-sitemap-page-item[data-id="'+dataId+'"]');
                data.sort = elem.index() + 1;
            }
			
			switch (addType) {
				// 新規のページを追加
				case 'create':
					url = '/site-map/api-create-page';
					data.page_type_code = $newPageTypeSelect.val();
					break;
				// 既存ページを追加
				case 'add':
					url = '/site-map/api-add-page';
					data.id = $editPageSelect.val();
					break;
				// リンクを追加
				case 'link':
					// 既存ページから選ぶ
					if ($linkPageModule.find('input[type="radio"]').prop('checked')) {
						url = '/site-map/api-create-alias';
						data.link_page_id = $linkPageSelect.val();
					}
					// URLからリンクを作る
					else {
						url = '/site-map/api-create-link';
						data.link_url = $linkUrlValue.val();
						data.title = $linkUrlTitle.val();
					}
					if ($linkUrlTargetBlank.prop('checked')) {
						data.link_target_blank = 1;
					}
					break;
				default:
					return;
			}
			
			app.api(url, data, function (res) {
				modal.clearErrors();
				
				if (res.errors) {
					app.modal.alert('', '登録内容に誤りがあります。');
					app.setErrors(modal.$el, res.errors);
					return;
				}
				if (typeof elem == 'undefined') {
                    sitemap.setData(res.items);
                } else {
                    sitemap.setData(res.items, elem);
                }
				modal.close();
			});
			
			return false;
		};
		
		return modal;
	};
	
	app.SiteMap.editLinkModal = function () {
		var contents = '<div class="modal-sitemap">' +
							'<dl>' +
								'<dt>既存ページから選ぶ</dt>' +
								'<dd>' +
									'<div>' +
										'<select></select>' +
									'</div>' +
									'<div><label><input type="checkbox" value="1" name="link_target_blank">別窓で開く</label></div>' +
								'</dd>' +
							'</dl>' +
							'<dl>' +
								'<dt>URLからリンクを作る</dt>' +
								'<dd>' +
									'<div class="category-select">' +
										'<label>URL</label>' +
										'<input id="link_url" type="text" name="link_url" maxlength="255" class="watch-input-count"><span class="input-count">0/255</span>' +
										'<div class="errors"></div>' +
									'</div>' +
									'<div class="type-select">' +
										'<label>リンク名</label>' +
										'<input id="title" type="text" name="title" maxlength="20" class="watch-input-count"><span class="input-count">0/30</span>' +
										'<div class="errors"></div>' +
									'</div>' +
									'<div><label><input type="checkbox" value="1" name="link_target_blank">別窓で開く</label></div>' +
								'</dd>' +
							'</dl>' +
						'</div>';
		var modal = app.modal.popup({
			title: 'リンクの編集',
			contents: contents,
			modalBodyInnerClass: 'align-top',
			autoRemove: false
		});
		
		var sitemap = app.SiteMap.getInstance();
		
		var $modules = modal.$el.find('.modal-sitemap dl');
		$modules.find('dt,dd').css('border-top', '0 none');
		var $linkPageModule = $modules.eq(0);
		var $linkPageSelect = $linkPageModule.find('select');
		var $linkUrlModule  = $modules.eq(1);
		var $linkUrlValue   = $linkUrlModule.find('input[name="link_url"]');
		var $linkUrlTitle   = $linkUrlModule.find('input[name="title"]');
		var $linkUrlTargetBlank = $modules.find('input[name="link_target_blank"]');
		
		modal.setLink = function (link) {
			modal.$el.find('.errors').empty();
			modal.$el.find('.is-error').removeClass('is-error');
			
			this.link = link;
			
			if (
				link.getType() === app.SiteMap.Types.TYPE_ALIAS ||
				link.getType() === app.SiteMap.Types.TYPE_ESTATE_ALIAS
			) {
				$linkPageModule.removeClass('is-hide');
				$linkUrlModule.addClass('is-hide');
				
				$linkPageSelect.empty();
				$.each(sitemap.getAllPages(), function (id, page) {
					if (page.canAlias()) {
						// リンク以外を選択肢に追加
						$linkPageSelect.append($('<option/>').attr('value', page.getLinkId()).text(page.getTitleWithFilename()));
					}
				});
				$linkPageSelect[0].selectedIndex = 0;
				$linkPageSelect.val(link.getType() === app.SiteMap.Types.TYPE_ESTATE_ALIAS ?
									link.data.link_estate_page_id:
									link.data.link_page_id);
			}
			else {
				$linkPageModule.addClass('is-hide');
				$linkUrlModule.removeClass('is-hide');
				
				$linkUrlValue.val(link.data.link_url || '').change();
				$linkUrlTitle.val(link.data.title || '').change();
			}
			$linkUrlTargetBlank.prop('checked', link.data.link_target_blank);
		};
		
		modal.onClose = function (ret, modal) {
			if (!ret) {
				return;
			}
			
			var url;
			
			var data = {};
			data._token = app.SiteMap.getInstance().token;
			data.id = modal.link.getId();
			
			if (
				modal.link.getType() === app.SiteMap.Types.TYPE_ALIAS ||
				modal.link.getType() === app.SiteMap.Types.TYPE_ESTATE_ALIAS
			) {
				url = '/site-map/api-update-alias';
				data.link_page_id = $linkPageSelect.val();
				if ($linkUrlTargetBlank.eq(0).prop('checked')) {
					data.link_target_blank = 1;
				}
			}
			else {
				url = '/site-map/api-update-link';
				data.link_url = $linkUrlValue.val();
				data.title = $linkUrlTitle.val();
				if ($linkUrlTargetBlank.eq(1).prop('checked')) {
					data.link_target_blank = 1;
				}
			}
			
			app.api(url, data, function (res) {
				modal.$el.find('.errors').empty();
				modal.$el.find('.is-error').removeClass('is-error');
				if (res.errors) {
					app.modal.alert('', '登録内容に誤りがあります。');
					app.setErrors(modal.$el, res.errors);
					return;
				}
				
				sitemap.setData(res.items);
				modal.close();
			});
			
			return false;
		};
		
		return modal;
	};
	
	app.SiteMap.Area = function ($container) {
		this.$container = $container;
		this.$add = $container.find('> .last');
	};
	app.SiteMap.Area.prototype = {
		getId: function () {
			return null;
		},
		
		inMenu: function () {
			return false;
		},
		
		empty: function () {
			this.$container.empty();
			if (this.$add.length) {
				this.$container.append(this.$add);
			}
			return this;
		},
		
		addChild: function (child, after) {
			if (child.parent) {
				child.parent.removeChild(child);
			}
			
			if (!this.$container.find('.app-sitemap-page-item[data-id="'+child.getId()+'"]').length) {
				if (this.$add.length) {
                    if (typeof after == 'undefined') {
                        this.$add.before(child.$el);
                    } else {
                        after.after(child.$el);
                    }
				}
				else {
					this.$container.append(child.$el);
				}
			}
			return this;
		},
		
		getChildren: function () {
			var children = [];
			this.$container.children(':not(.last)').each(function () {
				children.push( app.SiteMap.getInstance().getPage($(this).attr('data-id')) );
			});
			return children;
		},
		
		newSortNumber: function () {
			return 0;
		},
		
		sortChildren: function () {
			var children = this.getChildren();
			children.sort(this._sortChildrenMethod);
			
			var self = this;
			var $before;
			$.each(children, function (i, child) {
				if ($before) {
					$before.after(child.$el);
				}
				else {
					self.$container.prepend(child.$el);
				}
				$before = child.$el;
			});
			return this;
		},
		
		_sortChildrenMethod: function (a, b) {
			// type,id順
			if (a.getType() === b.getType()) {
				return a.getId() - b.getId();
			}
			else {
				return a.getType() - b.getType();
			}
		}
	};
	app.SiteMap.FixedArea = app.inherits(app.SiteMap.Area, function () {
		app.SiteMap.Area.apply(this, arguments);
	}, {
		getChildren: function () {
			var children = [];
			this.$container.children(':not(.last)').each(function () {
				if ($(this).attr('data-type') === 'estate_top') {
					children.push( app.SiteMap.getInstance().estatePages.top );
				}else if($(this).attr('data-type') === 'estate_rent') {
					children.push( app.SiteMap.getInstance().estatePages.chintai_top );
				}else if($(this).attr('data-type') === 'estate_purchase') {
					children.push( app.SiteMap.getInstance().estatePages.baibai_top );
				}else {
					children.push( app.SiteMap.getInstance().getPage($(this).attr('data-id')) );
				}
			});
			return children;
		},
		addChild: function (child) {
			if (child.parent) {
				child.parent.removeChild(child);
			}
			
			if (!this.$container.find('.app-sitemap-page-item[data-id="'+child.getId()+'"][data-type="'+child.getType()+'"]').length) {
				if (this.$add.length) {
					this.$add.before(child.$el);
				}
				else {
					this.$container.append(child.$el);
				}
			}
			return this;
		},
		sortChildren: function () {
			app.SiteMap.Area.prototype.sortChildren.call(this);
			// 物件ページ用処理
			this.$container.find('.clear').removeClass('clear');
			this.$container.find('[data-type="estate_top"]').prev().addClass('clear');
			return this;
		},
		_sortChildrenMethod: function (a, b) {
			// 物件ページ用処理
			// 物件ページのタイプを固定メニューでは扱わないタイプID30としてソートする
			if (a instanceof app.SiteMap.EstatePage) {
				return 30 - b.getType();
			}
			else if (b instanceof app.SiteMap.EstatePage) {
				return a.getType() - 30;
			}
			
			// type,id順
			if (a.getType() === b.getType()) {
				return a.getId() - b.getId();
			}
			else {
				return a.getType() - b.getType();
			}
		}
		
	});
	app.SiteMap.MenuArea = app.inherits(app.SiteMap.Area, function () {
			app.SiteMap.Area.apply(this, arguments);
		},
		{
			getId: function () {
				return 0;
			},
			
			inMenu: function () {
				return true;
			},
			
            getGlobalNumber: function () {
                return app.SiteMap.GlobalMenuNumber;
            },
            
			updateGlobal: function () {
                var _filterNumber = app.SiteMap.MenuArea.prototype.getGlobalNumber.call(this);
				this.$container.children().removeClass('global').filter(':lt('+_filterNumber+')').addClass('global');
				return this;
			},
			
			newSortNumber: function () {
				var _sortNumber = 0;
				$.each(this.getChildren(), function (i, page) {
					if (page.data.sort >= _sortNumber) {
						_sortNumber = page.data.sort + 1;
					}
				});
				
				return _sortNumber;
			},
			
			sortChildren: function () {
				app.SiteMap.Area.prototype.sortChildren.call(this);
				this.updateGlobal();
			},
			
			_sortChildrenMethod: function (a, b) {
				return a.data.sort - b.data.sort;
			}

		});
	
	app.SiteMap.Page = function (data) {
		this.setData(data || {});
		
		this.parent = null;
		this.children = [];
		
		this.$el = $('<li class="app-sitemap-page-item" data-id="'+this.getId()+'" data-type="'+this.getType()+'">' +
						'<div class="detail is-hide"></div>' +
						'<div class="item">' +
							'<div class="label">' +
								'<span class="status"></span>' +
								'<span class="type"></span>' +
							'</div>' +
							'<span class="page-name"></span>' +
							'<div class="action">' +
								'<a class="app-sitemap-page-item-edit" href="javascript:;"><i class="i-e-edit">編集</i></a>' +
								'<div class="pull">' +
									'<a href="javascript:;"><i class="i-e-set">操作</i></a>' +
									'<ul>' +
										'<li><a class="app-sitemap-page-item-new-page" href="javascript:;"><i class="i-e-up"></i>この下にページを追加</a></li>' +
										'<li><a class="app-sitemap-page-item-move" href="javascript:;"><i class="i-e-down"></i>順番を並び替える</a></li>' +
										'<li><a class="app-sitemap-page-item-remove" href="javascript:;"><i class="i-e-delete"></i>階層外へ移動させる</a></li>' +
									'</ul>' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<ul>' +
							'<li class="last is-hide"><div class="item add"><a href="javascript:;">追加</a></div></li>' +
						'</ul>' +
					'</li>');
		
		this.$item = this.$el.find('> .item');
		this.$childContainer = this.$el.find('> ul');
		this.$addBtn = this.$childContainer.find('.last');
		
	};
	
	app.SiteMap.Page.prototype = {
		getId: function () {
			return this.data.id;
		},
		
		getLinkId: function () {
			return this.data.link_id;
		},
		
		setData: function (data) {
			this.data = this.prepareData(data);
			return this;
		},
		
		updateData: function (data) {
			$.extend(this.data, this.prepareData(data));
			return this;
		},
		
		prepareData: function (data) {
			return data;
		},
		
		remove: function () {
			if (this.parent) {
				this.parent.removeChild(this);
			}
			else {
				this.$el.remove();
			}
			return this;
		},
		
		addChild: function (child, elem) {
			if (!child.parent || child.parent !== this) {
				
				if (child.parent) {
					child.parent.removeChild(child);
				}
                this.children.push(child);
                if (typeof elem == 'undefined') {
                    child.parent = this;
				    this.$childContainer.find('> .last').before(child.$el);
                } else {
                    elem.after(child.$el);
                }
			}
			
			return this;
		},
		
		removeChild: function (child) {
			var idx = app.arrayIndexOf(child, this.children);
			if (idx >= 0) {
				this.children.splice(idx, 1);
				child.$el.remove();
				child.parent = null;
			}
			return this;
		},
		
		getChildren: function () {
			return this.children;
		},
		
		newSortNumber: function () {
			var _sortNumber = 0;
			$.each(this.getChildren(), function (i, page) {
				if (page.data.sort >= _sortNumber) {
					_sortNumber = page.data.sort + 1;
				}
			});
			
			return _sortNumber;
		},
		
		getType: function () {
			return this.data.page_type_code;
		},
		
		getParentType: function () {
			return this.getType() - 1;
		},
		getTypeNameLabel: function(){
			if(this.isTopOriginal()){
					var data = this.data;

					var type = this.getType();
					if(type == app.SiteMap.Types.TYPE_INFO_INDEX || type == app.SiteMap.Types.TYPE_INFO_DETAIL){
            if(data.hasOwnProperty('text') && data.text){
            	return data.text;
						}
					}
			}
      return this.getTypeName();
		},
		isTopOriginal: function() {
			return app.SiteMap.IsTopOriginal;
		},
		isAgency: function() {
			return app.SiteMap.IsAgency;
		},
		getTypeName: function () {
			return app.SiteMap.getTypeName( this.getType() );
		},
		
		getChildTypes: function () {
			var level = this.getLevel();
			var childTypes = [];
			$.each(app.SiteMap.ChildTypes[ this.getType() ] || [], function (i, type) {
				if (app.SiteMap.hasDetailPageType(type) && level >= app.SiteMap.MaxLevel - 1) {
					return;
				}
				else if (level >= app.SiteMap.MaxLevel) {
					return;
				}
				childTypes.push(type);
			});
			return childTypes;
		},
		
		getParent: function () {
			if (this.parent) {
				return this.parent;
			}
			if (this.data.parent_page_id && app.SiteMap.getInstance().getPage(this.data.parent_page_id)) {
				return app.SiteMap.getInstance().getPage(this.data.parent_page_id);
			}
			return null;
		},
		
		canAddChild: function () {
			return !!this.getChildTypes().length && !this.isTopType() && !this.isMultiIndex() && this.inMenu();
		},
		
		canAlias: function () {
			return !this.isLink() && !this.isNew() && !this.isEstateForm();
		},
		
		getTitle: function () {
			if (this.getType() === app.SiteMap.Types.TYPE_ALIAS) {
				var linkPage = app.SiteMap.getInstance().getPageByLinkId(this.data.link_page_id);
				if (linkPage) {
					return linkPage.getTitle();
				}
			}
			else if (this.getType() === app.SiteMap.Types.TYPE_ESTATE_ALIAS) {
				var linkPage = app.SiteMap.getInstance().getPageByEstateLinkId(this.data.link_estate_page_id);
				if (linkPage) {
					return linkPage.getTitle();
				}
			}
			else if (this.isMultiDetail()) {
				return this.getTypeName() + '（' + this.data.count + '件）';
			}
			return this.data.title || '';
		},
		
		getTitleWithFilename: function () {
			var filename = this.getFilename();
			if (filename) {
				filename = '（'+filename+'）';
			}
			return this.getTitle() + filename;
		},
		
		getFilename: function () {
			return this.isNew() || !this.data.filename ? '' : this.data.filename;
		},
		
		isTopType: function () {
			return this.getType() === app.SiteMap.Types.TYPE_TOP;
		},
		
		isFixedType: function () {
			return app.SiteMap.isFixedType(this.getType());
		},
		
		isLink: function () {
			return app.SiteMap.getCategoryFromType(this.getType()) === app.SiteMap.Categories.CATEGORY_LINK;
		},
		
		isEstateForm: function () {
			var type = this.getType();
			return (
					type == app.SiteMap.Types.TYPE_FORM_LIVINGLEASE ||
					type == app.SiteMap.Types.TYPE_FORM_OFFICELEASE ||
					type == app.SiteMap.Types.TYPE_FORM_LIVINGBUY ||
					type == app.SiteMap.Types.TYPE_FORM_OFFICEBUY
			);
		},
		
		hasDetailPageType: function () {
			return app.SiteMap.hasDetailPageType(this.getType());
		},
		
		isDetailPageType: function () {
			return app.SiteMap.isDetailPageType(this.getType());
		},
		
		/**
		 * 複数ページかどうか（ブログ等）
		 */
		isMultiIndex: function () {
			return app.SiteMap.hasMultiPageType(this.getType());
		},
		
		isMultiDetail: function () {
			var parent = this.getParent();
			return !!parent && parent.isMultiIndex();
		},
		
		isNew: function () {
			return this.data.new_flg;
		},
		
		/**
		 * @todo 
		 */
		isDraft: function () {
			return !this.data.public_flg;
		},
		
		isPublic: function () {
			return this.data.public_flg;
		},
		
		getItemClass: function () {
			if (this.isLink()) {
				return 'is-link';
			}
			
			var cls = [];
			if (this.isNew()) {
				cls.push('is-empty');
			}
			if (this.isDraft()) {
				cls.push('is-draft');
			}
			
			if (this.isMultiDetail()) {
				cls.push('is-multi-detail');
			}
			
			return cls.join(' ');
		},
		
		inMenu: function () {
			return !isNaN(parseInt(this.data.parent_page_id));
		},
		
        isGlobal: function(pageId) {
            var globalNav = false;
            $.each(app.SiteMap.globalNav, function(index, value) {
                if (value["id"] == pageId) {
                    globalNav = true;
                }
            });
            return globalNav;
        },
		
		hasEditMenu: function () {
			if (this.isTopOriginal() && !this.isAgency()) {
				return !this.isTopType() && this.inMenu() && !this.isMultiDetail() && !this.isGlobal(this.getId());
			}

			return !this.isTopType() && this.inMenu() && !this.isMultiDetail();
		},
		
		getLevel: function () {
			var parent = this.getParent();
			if (parent) {
				return parent.getLevel() + 1;
			}
			else {
				return 1;
			}
		},
		
		render: function (recursive) {
			// 会員専用ページ、公開中のリンクはメニューから削除付加
			if (
				(this.getType() === app.SiteMap.Types.TYPE_MEMBERONLY) ||
				(this.isLink() && this.isPublic())
			) {
				this.$el.find('> .item .app-sitemap-page-item-remove').parent().remove();
			}
			
			// ページ情報
			this.$el.attr({'data-id': this.getId(), 'data-type': this.getType()});
			// 詳細まとめ一覧の場合、詳細を追加
			if (this.isMultiIndex()) {
				if (!this.children.length) {
					this.addChild((new app.SiteMap.Page(this.data.detail)).render());
				}
			}
			// 詳細まとめ表示
			this.$item.prev().toggleClass('is-hide', !this.isMultiDetail());
			// 状態
			this.$item.attr('class', 'item ' + this.getItemClass());
			// TOP個別クラス
			this.$item.toggleClass('home', this.isTopType());
			// ページタイプ名
			this.$item.find('.type').text(this.getTypeNameLabel());
			// ページ名
			this.$item.find('.page-name').text(this.getTitle());
			// 一覧編集ボタン
			this.$item.find('.action > a i')
				.attr('class', this.isMultiIndex() ? 'i-e-list' : 'i-e-edit')
				.toggleClass('is-hide', this.isMultiDetail());
			// 設定ボタン表示
			this.$item.find('.action .pull').toggleClass('is-hide', !this.hasEditMenu());
			// 階層クラス
			this.$childContainer.attr('class', 'level' + (this.getLevel() + 1));
			// 追加ボタン
			this.$addBtn.toggleClass('is-hide', !this.canAddChild());
			this.$addBtn.siblings().removeClass('last');
			if (!this.canAddChild()) {
				this.$addBtn.prev().addClass('last');
			}
			
			if (recursive) {
				$.each(this.children, function (i, child) {
					child.render(true);
				});
			}
			
			return this;
		}
	};
	
	
	app.SiteMap.EstatePage = app.inherits(app.SiteMap.Page, function () {
		app.SiteMap.Page.apply(this, arguments);
	},
	{
		getType: function () {
			return this.data.estate_page_type;
		},
		isEstateTop: function () {
			return this.data.estate_page_type === 'estate_top';
		},
		isEstateRent: function () {
			return this.data.estate_page_type === 'estate_rent';
		},
		isEstatePurchase: function () {
			return this.data.estate_page_type === 'estate_purchase';
		},
		/**
		 * 物件種目ページかどうか
		 */
		isEstateType: function () {
			return this.data.estate_page_type === 'estate_type';
		},
		isEstateSpecial: function () {
			return this.data.estate_page_type === 'estate_special';
		},
		isPublic: function () {
			return this.data.public_flg;
		},
		isDraft: function () {
			return !this.isPublic();
		},
		getItemClass: function () {
			var cls = [];
			if (this.isDraft()) {
				cls.push('is-draft');
			}
			return cls.join(' ');
		},
		getTypeName: function () {
			if (this.data.estate_page_type === 'estate_special') {
				return '特集';
			}
			else {
				return this.getTitle();
			}
		},
		getLevel: function () {
			if (this.isEstateType()) {
				return 2;
			}
			else {
				return 1;
			}
		},
		getEditPageUrl: function () {
			if (this.isEstateTop() || this.isEstateRent() || this.isEstatePurchase()) {
				return '/estate-search-setting';
			}
			else if (this.isEstateType()) {
				return '/estate-search-setting/detail?class='+this.data.estate_class;
			}
			else if (this.isEstateSpecial()) {
				return '/estate-special/detail?id='+this.data.id;
			}
			return false;
		},
		render: function (recursive) {
			// 物件検索用クラス
			this.$el.addClass('app-sitemap-estate-page-item');
			// メニューから削除付加
			this.$el.find('> .item .app-sitemap-page-item-remove').parent().remove();
			// 物件検索トップ用クラス
			this.$el.toggleClass('article-search', this.isEstateTop());
			
			// ページ情報
			//this.$el.attr({'data-id': this.getId(), 'data-type': this.getType()});
			// 状態
			this.$item.attr('class', 'item ' + this.getItemClass());
			// ページタイプ名
			this.$item.find('.type').text(this.getTypeName());
			// ページ名
			this.$item.find('.page-name').text(this.getTitle());
			// 一覧編集ボタン
			this.$item.find('.action > a i')
				.attr('class', 'i-e-edit');
			// 設定ボタン表示
			this.$item.find('.action .pull').toggleClass('is-hide', true);
			// 階層クラス
			this.$childContainer.attr('class', 'level' + (this.getLevel() + 1));
			// 追加ボタン
			this.$addBtn.toggleClass('is-hide', true);
			this.$addBtn.siblings().removeClass('last');
			this.$addBtn.prev().addClass('last');
			
			if (recursive) {
				$.each(this.children, function (i, child) {
					child.render(true);
				});
			}
			
			return this;
		}
	});


})(app);
